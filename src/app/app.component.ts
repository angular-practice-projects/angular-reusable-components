import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  post={
    title:'Reusable Components',
    isFavorite:true
  };

  onFavoriteChange(isFavorite){
    console.log('favorite changed - '+isFavorite)
  }
}
