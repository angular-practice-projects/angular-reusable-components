import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css'],
})
export class FavouriteComponent  {
  @Input('is-favorite')
  isFavorite:boolean;

  @Output('changed')
  change = new EventEmitter();

  onClick(){
    this.isFavorite = !this.isFavorite;
    this.change.emit(this.isFavorite);
  }
}
